class Queue{
    private var queue:MutableList<String> = mutableListOf()

    fun enqueue(item: String){
        queue.add(item)
    }

    fun dequeue(){
        if (queue.any())
            queue.removeAt(0)
    }

    fun getQueue():MutableList<String>{
        return queue
    }
}