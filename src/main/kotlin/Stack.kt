class Stack{
    private var stack:MutableList<String> = mutableListOf()

    fun push(item: String){
        stack.add(item)
    }

    fun pop() {
        if (stack.any())
            stack.removeAt(stack.size - 1)
    }

    fun getStack():MutableList<String>{
        return stack
    }
}