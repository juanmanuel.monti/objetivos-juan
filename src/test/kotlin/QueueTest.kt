import junit.framework.Assert.assertEquals
import org.junit.Test

class QueueTest {

    private lateinit var games:Queue
    var firstGame = "Resident evil"
    var secondGame = "Silent Hill"
    var thirdGame = "Alone In The Dark"

    @Test
    fun `add an element`() {
        givenAQueue()
        whenAdds()
        thenElementIsAdded()
    }

    @Test
    fun `add more than one element`() {
        givenAQueue()
        whenAddsMoreThanOneElement()
        thenElementsAreAdded()
    }

    @Test
    fun `remove an element`() {
        givenAQueueWithAnElement()
        whenRemoves()
        thenElementIsRemoved()
    }

    @Test
    fun `ensure that removes the first added element`() {
        givenAQueueWithTwoElements()
        whenRemoves()
        thenCorrectElementIsRemoved()
    }

    @Test
    fun `multiple removes and adds`() {
        givenAQueue()
        whenEnqueueAndDequeue()
        thenCorrectElementIsRemaining()
    }

    private fun givenAQueue() {
        games = Queue()
    }

    private fun givenAQueueWithAnElement() {
        givenAQueue()
        games.enqueue(firstGame)
    }

    private fun givenAQueueWithTwoElements() {
        givenAQueueWithAnElement()
        games.enqueue(secondGame)
    }

    private fun whenAdds() {
        games.enqueue(firstGame)
    }

    private fun whenAddsMoreThanOneElement() {
        games.enqueue(firstGame)
        games.enqueue(secondGame)
    }

    private fun whenRemoves() {
        games.dequeue()
    }

    private fun thenElementIsAdded() {
        val queue = games.getQueue()
        assertEquals(listOf(firstGame), queue)
    }

    private fun thenElementsAreAdded() {
        val queue = games.getQueue()
        assertEquals(listOf(firstGame,secondGame), queue)
    }

    private fun thenElementIsRemoved() {
        val queue = games.getQueue()
        assertEquals(listOf<String>(),queue)
    }

    private fun thenCorrectElementIsRemoved() {
        val queue = games.getQueue()
        assertEquals(listOf(secondGame), queue)
    }

    private fun whenEnqueueAndDequeue() {
        games.enqueue(firstGame)
        games.enqueue(secondGame)
        games.dequeue()
        games.enqueue(thirdGame)
        games.dequeue()
    }

    private fun thenCorrectElementIsRemaining() {
        val queue = games.getQueue()
        assertEquals(listOf(thirdGame), queue)
    }
}