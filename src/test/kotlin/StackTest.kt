import junit.framework.Assert.assertEquals
import org.junit.Test

class StackTest {

    lateinit var games:Stack
    var firstGame = "Resident evil"
    var secondGame = "Resident evil 2"
    var thirdGame = "Alone In The Dark"

    @Test
    fun `add an element`() {
        givenAStack()
        whenAdds()
        thenElementIsAdded()
    }

    @Test
    fun `add more than one element`() {
        givenAStack()
        whenAddsMoreThanOne()
        thenElementsAreAdded()
    }

    @Test
    fun `remove an element`() {
        givenAStackWithAnElement()
        whenRemoves()
        thenElementIsRemoved()
    }

    @Test
    fun `ensure that removes the last added element`() {
        givenAStackWithMoreThanOneElement()
        whenRemoves()
        thenCorrectElementIsRemove()
    }

    @Test
    fun `multiple removes and adds`() {
        givenAStack()
        whenPushAndPop()
        thenCorrectElementIsRemaining()
    }

    private fun givenAStack(){
        games = Stack()
    }

    private fun givenAStackWithAnElement() {
        givenAStack()
        games.push(firstGame)
    }

    private fun givenAStackWithMoreThanOneElement() {
        givenAStackWithAnElement()
        games.push(secondGame)
    }

    private fun whenAdds(){

        games.push(firstGame)
    }

    private fun whenAddsMoreThanOne() {
        games.push(firstGame)
        games.push(secondGame)
    }

    private fun whenRemoves() {
        games.pop()
    }

    private fun whenPushAndPop() {
        games.push(firstGame)
        games.push(secondGame)
        games.pop()
        games.push(thirdGame)
        games.pop()
    }

    private fun thenElementIsAdded() {
        val queue = games.getStack()
        assertEquals(listOf(firstGame), queue)
    }

    private fun thenElementsAreAdded() {
        val queue = games.getStack()
        assertEquals(listOf(firstGame,secondGame), queue)
    }

    private fun thenElementIsRemoved() {
        val queue = games.getStack()
        assertEquals(listOf<String>(),queue)
    }

    private fun thenCorrectElementIsRemove() {
        val queue = games.getStack()
        assertEquals(listOf(firstGame), queue)
    }

    private fun thenCorrectElementIsRemaining() {
        val queue = games.getStack()
        assertEquals(listOf(firstGame), queue)
    }
}